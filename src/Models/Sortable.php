<?php

namespace Mosamy\Sortable\Models;

use Illuminate\Database\Eloquent\Model;

class Sortable extends Model
{
  protected $guarded = [];
  public $timestamps = false;

  public function sortable(){
    return $this->morphTo();
  }

}
