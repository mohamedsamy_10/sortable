<?php

use Illuminate\Support\Facades\Route;

Route::middleware(config('sort.middleware'))->group(function(){
  Route::post(config('sort.url'), \Mosamy\Sortable\Http\Controllers\SortController::class);
});
