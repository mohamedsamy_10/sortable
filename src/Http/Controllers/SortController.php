<?php

namespace Mosamy\Sortable\Http\Controllers;

use App\Http\Controllers\Controller;
use Mosamy\Sortable\Http\Requests\SortRequest;
use Mosamy\Sortable\Models\Sortable;

class SortController extends Controller
{

    public function __invoke(SortRequest $request){

        $sortable_type = 'App\Models\\'.$request->sortable_type;
        $sortables = collect($request->sortable_ids)->filter()->unique()
        ->map(function($sortable_id) use($sortable_type){
          return [
            'sortable_id' => $sortable_id,
            'sortable_type' => $sortable_type
          ];
        })->toArray();

        Sortable::where('sortable_type', $sortable_type)
        ->whereIn('sortable_id', $request->sortable_ids)
        ->delete();
		
        Sortable::insert($sortables);
        $sortable_type::afterSort();

    }
}
