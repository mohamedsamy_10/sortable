$(document).ready(function() {

    var tableClass = 'sortable-table';

    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width())
        });
        return $helper;
    };

    $("." + tableClass + " tbody").sortable({
        helper: fixHelperModified,
        stop: function(event, ui) {
            renumber_table('.' + tableClass)
        }
    }).disableSelection();

    function renumber_table(tableID) {
        sortable_ids = [];
        sortable_type = $('#sortable-type').val();

        $(tableID + " tr").each(function() {
          sortable_ids.push($(this).find('.sortable-id').val());
          sortable_ids = sortable_ids.filter(Number);
        });
        $.post(sort_url, {sortable_ids: sortable_ids, sortable_type:sortable_type});
    }

});
