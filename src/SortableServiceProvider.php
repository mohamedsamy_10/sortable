<?php

namespace Mosamy\Sortable;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class SortableServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
      $this->mergeConfigFrom(__DIR__.'/Config.php', 'sort');
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
      
      Blade::directive('sortJs', function () {
          return '
          <script type="text/javascript">
            var sort_url = "'.url(config('sort.url')).'";
          </script>
          <script src="'.asset('js/draggable.js').'"></script>
          ';
      });

      //php artisan vendor:publish --provider="Mosamy\Sortable\SortableServiceProvider" --tag="config"
      if ($this->app->runningInConsole()) {
        $this->publishes([
          __DIR__.'/Config.php' => config_path('sort.php'),
        ], 'config');
      }

      //php artisan vendor:publish --provider="Mosamy\Sortable\SortableServiceProvider" --tag="assets"
      $this->publishes([
        __DIR__.'/Js/draggable.js' => public_path('js/draggable.js'),
      ], 'assets');

      $this->loadMigrationsFrom(__DIR__.'/Database/migrations/');
      $this->loadRoutesFrom(__DIR__.'/Http/Routes/web.php');
      $this->loadViewsFrom(__DIR__ . '/views', 'sortable');
    }
}
