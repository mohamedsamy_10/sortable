<?php

namespace Mosamy\Sortable;

use Mosamy\Sortable\Models\Sortable as SortableModel;

trait Sortable
{

  public static function bootSortable(){
    static::deleting(function ($model) {
      $model->sort()->delete();
    });
  }

  public function sort(){
    return $this->morphOne(SortableModel::class, 'sortable');
  }

  public static function afterSort(){
    // add some code to execute after the sort is completed
  }

  public function scopeOrderBySort($query){
    $table = with(new static)->getTable();
    $model = class_basename(with(new static));

    $query->addSelect(['sort' => SortableModel::select('id')
      ->whereColumn('sortable_id', $table.'.id')
      ->where('sortable_type', 'App\Models\\'.$model)
    ])
    ->orderBy('sort');
  }
}


?>
